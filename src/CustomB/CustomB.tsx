import React from 'react';
import { Button } from 'react-native';
import { useDispatch } from 'react-redux';
import { connect } from '../../store/actions/connected';


const CustomB: React.FC = () => {
    const dispatch = useDispatch()
    return <Button title="Connect" onPress={() => dispatch(connect())}></Button>
}

export default CustomB;