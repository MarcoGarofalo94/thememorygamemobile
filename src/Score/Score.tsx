import React from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../App";
import { ScreenState } from "../../store/types/screen";
import { FontAwesome5 } from "@expo/vector-icons";
import { View } from "react-native";

interface ScoreProps {}

const Score: React.FC<ScoreProps> = ({}) => {


  const { score } = useSelector<RootState, ScreenState>(
    (state) => state.screen
  );

  return (
    <View
      style={{
        display: "flex",
        flexDirection: "row",
        margin: 30,
        flexWrap: "wrap",
        alignItems:'center',
       
      }}
    >
      {score.map((brain,i) => {
        return <FontAwesome5 style={{margin: 5}} key={i} name="brain" size={24} color="pink" />;
      })}
    </View>
  );
};


export default Score;