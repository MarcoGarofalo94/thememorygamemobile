import axios from "axios";
import React from "react";
import { ActivityIndicator, Button, StyleSheet, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../App";
import { resetError, resetMatrix,submitMatrix } from "../../store/actions/screen";
import { ScreenState } from "../../store/types/screen";
import {logout} from '../../store/actions/connected';
import LED from "../LED/LED";
import Score from "../Score/Score";
import { ConnectionState } from "../../store/types/connected";

interface MatrixProps {}

const Matrix: React.FC<MatrixProps> = ({}) => {
  const dispatch = useDispatch();
  const { matrix,score,error,isLoading } = useSelector<RootState, ScreenState>(
    (state) => state.screen
  );

  const { ip } = useSelector<RootState, ConnectionState>(
    (state) => state.connection
  );

  React.useEffect(( ) => {
    if(error){
      dispatch(logout());
      dispatch(resetError())
    }
  },[error])
  return (
    <View
      style={{
        display: "flex",
        justifyContent: "center",
        marginTop: 100,
        alignItems: "center",
      }}
    >
      <Text style={{ color: "white", fontSize: 24 }}>Your Score:</Text>
      <Score />
      <View
        style={{
          flex: 1,
          display: "flex",

          flexWrap: "wrap",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-evenly",
          padding: 30,
        }}
      >
        {matrix.map((row, rowIndex) => {
          return (
            <View
              style={{ display: "flex", flexDirection: "row", padding: 0 }}
              key={rowIndex}
            >
              {row.map((column, colIndex) => {
                return (
                  <LED
                    matrix={matrix}
                    rowIndex={rowIndex}
                    colIndex={colIndex}
                    isActive={column}
                    key={colIndex + " s"}
                  >
                    <Text>{rowIndex + "" + colIndex + " "}</Text>
                  </LED>
                );
              })}
              <Text>{"\n"}</Text>
            </View>
          );
        })}
      </View>
      {isLoading && <ActivityIndicator size={"large"} color="pink" />}
      <View
        style={{
          width: "100%",
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-evenly",
          marginVertical: 50,
        }}
      >
        <View style={{ width: "40%" }}>
          <Button
            title="Reset"
            color="tomato"
            onPress={() => dispatch(resetMatrix())}
          />
        </View>
        <View style={{ width: "40%" }}>
          <Button title="Send" onPress={() => dispatch(submitMatrix(matrix,score,ip)) /*axios.post('http://192.168.1.99',{rows: ['10010001','00000001']})*/} />
        </View>

      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  MatrixContainer: {
    backgroundColor: "#414141",
  },
});
export default Matrix;
