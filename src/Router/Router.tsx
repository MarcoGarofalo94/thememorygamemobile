import React from "react";
import { ActivityIndicator, ActivityIndicatorComponent, Dimensions, Text, View } from "react-native";
import { useSelector } from "react-redux";
import { RootState } from "../../App";
import { ConnectionState } from "../../store/types/connected";
import CustomB from "../CustomB/CustomB";
import Matrix from "../Matrix/Matrix";
import { FontAwesome5 } from "@expo/vector-icons";
const Router: React.FC = () => {
  const { isConnected, isLoading,ip,error } = useSelector<RootState, ConnectionState>(
    (state) => state.connection
  );

  return isConnected ? (
    <Matrix />
  ) : (
    <View style={{display: "flex",justifyContent:'flex-start',alignItems:'center', width: Dimensions.get("screen").width, height: "100%",marginTop: 200}}>
      <Text style={{color: "white", fontSize: 48,textAlign:'center'}}>The Memory Game</Text>
      <FontAwesome5 style={{marginVertical: 40}}  name="brain" size={100} color="pink" />
      
     <CustomB />
      { ip == null && <Text style={{color: "tomato"}}>
        Impossible to connect.
      </Text> }
     {isLoading && <ActivityIndicator size="large" color="pink"/>}

    </View>
  );
};

export default Router;
