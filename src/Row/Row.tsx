import React from "react";
import { Text, View } from "react-native";

interface RowProps {
}

const Row: React.FC<RowProps> = ({children }) => {
  return (
    <View>
      <Text>{children}</Text>
    </View>
  );
};

export default Row;
