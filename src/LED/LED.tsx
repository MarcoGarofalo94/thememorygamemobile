import React from "react";
import {
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { useDispatch } from "react-redux";
import { updateMatrix } from "../../store/actions/screen";

interface LEDProps {
  isActive: boolean;
  matrix: boolean[][];
  rowIndex: number;
  colIndex: number;
}

const LED: React.FC<LEDProps> = ({
  children,
  isActive,
  matrix,
  rowIndex,
  colIndex,
}) => {
  const dispatch = useDispatch();
  return (
    <TouchableOpacity
      onPress={() => dispatch(updateMatrix(matrix, rowIndex, colIndex, !isActive))}
    >
      <View style={styles.LEDContainer}>
        <View
          style={{
            ...styles.LED,
            backgroundColor: isActive ? "tomato" : "gray",
          }}
        ></View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  LEDContainer: {
    display: "flex",
    padding: 7,
    height: 40,
    width: 40,
  },
  LED: {
    width: "100%",
    height: "100%",
    borderRadius: 20,
  },
});

export default LED;
