import {
  combineReducers,
  createStore,
  applyMiddleware,
} from "@reduxjs/toolkit";
import ReduxThunk from "redux-thunk";
import * as Network from "expo-network";
import React from "react";
import { StyleSheet, Text, View, Button } from "react-native";
import { Provider } from "react-redux";
import screenReducer from "./store/reducers/screen";
import Matrix from "./src/Matrix/Matrix";
import connectedReducer from "./store/reducers/connected";
import { connect } from "./store/actions/connected";
import CustomB from "./src/CustomB/CustomB";
import Router from "./src/Router/Router";
const rootReducer = combineReducers({
  screen: screenReducer,
  connection: connectedReducer,
});
const store = createStore(rootReducer, applyMiddleware(ReduxThunk));
export type RootState = ReturnType<typeof rootReducer>;
export default function App() {
  //Network.getIpAddressAsync().then(ip=> console.log(ip))
  //Network.getNetworkStateAsync().then(ip=> console.log(ip))

  return (
    <Provider store={store}>
      <View style={styles.container}>
        <Router />
      </View>
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#414141",
    alignItems: "center",
    justifyContent: "center",
  },
});
