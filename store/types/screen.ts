export const SUBMIT_MATRIX_START = "SUBMIT_MATRIX_START";
export const SUBMIT_MATRIX_FAIL = "SUBMIT_MATRIX_FAIL";
export const SUBMIT_MATRIX_SUCCESS = "SUBMIT_MATRIX_SUCCESS";

export const UPDATE_MATRIX = "UPDATE_MATRIX";

export const RESET_MATRIX = "RESET_MATRIX";

export const RESET_ERROR = "RESET_ERROR";

export interface ScreenState {
  matrix: boolean[][];
  score: number[];
  connected: boolean;
  isLoading: boolean;
  error: null | Error;
}

interface UpdateMatrixAction {
  type: typeof UPDATE_MATRIX;
  matrix: boolean[][];
}

interface ResetMatrixAxion {
  type: typeof RESET_MATRIX;
  matrix: boolean[][];
}

interface SubmitMatrixStartAction {
  type: typeof SUBMIT_MATRIX_START;
}

interface SubmitMatrixFailAction {
  type: typeof SUBMIT_MATRIX_FAIL;
  error: null | Error;
}

interface SubmitMatrixSuccessAction {
  type: typeof SUBMIT_MATRIX_SUCCESS;
  score: number[];
}

interface ResetErrorAction {
  type: typeof RESET_ERROR;
}

export type ScreenActions =
  | UpdateMatrixAction
  | ResetMatrixAxion
  | SubmitMatrixStartAction
  | SubmitMatrixFailAction
  | SubmitMatrixSuccessAction
  | ResetErrorAction;
