export const CONNECTION_START = "CONNECTION_START";
export const CONNECTION_FAIL = "CONNECTION_FAIL";
export const CONNECTION_SUCCESS = "CONNECTION_SUCCESS";
export const LOGOUT = "LOGOUT";

export interface ConnectionState {
  isLoading: boolean;
  error: null | Error;
  isConnected: boolean;
  ip: string | null;
}

interface ConnectionStartAction {
  type: typeof CONNECTION_START;
}

interface ConnectionFailAction {
  type: typeof CONNECTION_FAIL;
  error: null | Error;
}

interface LogoutAction {
  type: typeof LOGOUT;
}

interface ConnectionSuccessAction {
  type: typeof CONNECTION_SUCCESS;
  ip: string;
  isConnected: boolean;
}

export type ConnectionActionTypes =
  | ConnectionStartAction
  | ConnectionFailAction
  | ConnectionSuccessAction
  | LogoutAction;
