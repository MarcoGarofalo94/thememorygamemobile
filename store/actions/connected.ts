import axios from "axios";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import {
  ConnectionActionTypes,
  CONNECTION_FAIL,
  CONNECTION_START,
  CONNECTION_SUCCESS,
  LOGOUT,
} from "../types/connected";

export const connect = (): ThunkAction<
  Promise<void>,
  {},
  {},
  ConnectionActionTypes
> => {
  return async (dispatch: ThunkDispatch<{}, {}, ConnectionActionTypes>) => {
    dispatch(connectionStart());

    try {
      const response = await axios.get("http://90.147.166.236:8082/connect");
    let ip = response.data.ip;
      dispatch(connectionSuccess(ip != null ? ip : null ,response.data.ip != null));
      console.log(ip != null)
    } catch (error) {
      console.log(error);
      dispatch(connectionFail(error));
    }
  };
};

const connectionStart = (): ConnectionActionTypes => {
  return { type: CONNECTION_START };
};

const connectionFail = (error: null | Error): ConnectionActionTypes => {
  return { type: CONNECTION_FAIL, error: error };
};
const connectionSuccess = (ip: string,isConnected: boolean): ConnectionActionTypes => {
  return { type: CONNECTION_SUCCESS, ip: ip,isConnected: isConnected };
};

export const logout = (): ConnectionActionTypes => {
  return {type: LOGOUT}
}