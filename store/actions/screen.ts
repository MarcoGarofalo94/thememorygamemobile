import axios from "axios";
import { ThunkAction, ThunkDispatch } from "redux-thunk";
import { RootState } from "../../App";
import {
  UPDATE_MATRIX,
  ScreenActions,
  ScreenState,
  RESET_MATRIX,
  SUBMIT_MATRIX_START,
  SUBMIT_MATRIX_FAIL,
  SUBMIT_MATRIX_SUCCESS,
  RESET_ERROR,
} from "../types/screen";

export const updateMatrix = (
  matrix: boolean[][],
  rowIndex: number,
  colIndex: number,
  isActive: boolean
): ScreenActions => {
  const newMatrix = [...matrix];
  newMatrix[rowIndex][colIndex] = isActive;

  return { type: UPDATE_MATRIX, matrix: newMatrix };
};

export const resetMatrix = (): ScreenActions => {
  const matrix = [
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
  ];
  return { type: RESET_MATRIX, matrix: matrix };
};

const submitMatrixStart = (): ScreenActions => {
  return { type: SUBMIT_MATRIX_START };
};

const submitMatrixFail = (error: null | Error): ScreenActions => {
  return { type: SUBMIT_MATRIX_FAIL, error: error };
};

const submitMatrixSuccess = (score: number[]): ScreenActions => {
  return { type: SUBMIT_MATRIX_SUCCESS, score: score };
};

export const submitMatrix = (
  matrix: boolean[][],
  score: number[],
  ip: string | null
): ThunkAction<Promise<void>, {}, {}, ScreenActions> => {
  return async (dispatch: ThunkDispatch<{}, {}, ScreenActions>, getState) => {
    dispatch(submitMatrixStart());
    const binaryRows: string[] = [];
    matrix.forEach((row) => binaryRows.push(boolToBinaryString(row)));

    const state = getState() as RootState;
    console.log("action state", state.connection.ip);


    try {

      const response = await axios.post("http://"+state.connection.ip as string, {
        rows: binaryRows,
      });
      console.log(binaryRows);
      let newScore = [...score];
      console.log(response.data);
      if (response.data.correct) {
        newScore.push(0);
      } else {
        newScore = [];
      }
      dispatch(submitMatrixSuccess(newScore));
    } catch (error) {
      console.log(error);
      dispatch(submitMatrixFail(error));
    }
  };
};

const boolToBinaryString = (row: boolean[]) => {
  let binaryString = "";
  row.forEach((bit, index) => {
    binaryString += bit ? "1" : "0";
  });
  //console.log(binaryString);
  return binaryString;
};

export const resetError = (): ScreenActions => {
  return { type: RESET_ERROR };
};
