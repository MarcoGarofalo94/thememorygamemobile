import {
  ConnectionActionTypes,
  ConnectionState,
  CONNECTION_SUCCESS,
  CONNECTION_FAIL,
  CONNECTION_START,
  LOGOUT,
} from "../types/connected";

const initialState: ConnectionState = {
  isConnected: false,
  isLoading: false,
  error: null,
  ip: null,
};

const connectedReducer = (
  state = initialState,
  action: ConnectionActionTypes
): ConnectionState => {
  switch (action.type) {
    case CONNECTION_START:
      return { ...state, isLoading: true, ip: null, error: null, isConnected: false };
    case CONNECTION_FAIL:
      return { ...state, isLoading: false, error: action.error, isConnected: false};
    case CONNECTION_SUCCESS:
      return { ...state, isLoading: false,ip: action.ip, isConnected: action.isConnected };
    case LOGOUT:
      return {...state, ip: null, isConnected: false}
    default:
      return state;
  }
};

export default connectedReducer;
