import {
  UPDATE_MATRIX,
  ScreenActions,
  ScreenState,
  RESET_MATRIX,
  SUBMIT_MATRIX_START,
  SUBMIT_MATRIX_FAIL,
  SUBMIT_MATRIX_SUCCESS,
  RESET_ERROR,
} from "../types/screen";

const initialState: ScreenState = {
  error: null,
  connected: false,
  isLoading: false,
  score: [],
  matrix: [
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
    [false, false, false, false, false, false, false, false],
  ],
};

const screenReducer = (
  state = initialState,
  action: ScreenActions
): ScreenState => {
  switch (action.type) {
    case UPDATE_MATRIX:
      return { ...state, matrix: action.matrix };
    case RESET_MATRIX:
      return { ...state, matrix: action.matrix };
    case SUBMIT_MATRIX_START:
      return { ...state, isLoading: true, error: null};
    case SUBMIT_MATRIX_FAIL:
      return { ...state, isLoading: false, error: action.error };
    case SUBMIT_MATRIX_SUCCESS:
      return { ...state, isLoading: false ,score: action.score, error:null};
      case RESET_ERROR:
        return {...state, isLoading: false, error: null};
    default:
      return state;
  }
};

export default screenReducer;
